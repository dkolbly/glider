package main

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"strings"
	"time"

	"github.com/dkolbly/cli"
	"github.com/dkolbly/logging"
	_ "github.com/dkolbly/logging/pretty"
	"gopkg.in/yaml.v2"
)

var log = logging.New("glider")
var gopath string

type Imported struct {
	Name    string `yaml:"name"`
	Version string `yaml:"version"`
	Repo    string `yaml:"repo"`
}

func (imp *Imported) useRepo() string {
	if imp.Repo != "" {
		return imp.Repo
	}
	return fmt.Sprintf("https://%s.git", imp.Name)
}

type LockFile struct {
	Hash    string     `yaml:"hash"`
	Updated time.Time  `yaml:"updated"`
	Imports []Imported `yaml:"imports"`
}

func main() {
	app := &cli.App{
		Name:    "glider",
		Version: "0.1.0",
		Usage:   "Manage GOPATH and glide.lock file",
		Commands: []*cli.Command{
			statusCmd,
			checkoutCmd,
		},
	}
	app.Run(os.Args)
}

var statusCmd = &cli.Command{
	Name:   "status",
	Usage:  "check whether GOPATH is in sync with glide.lock",
	Action: statusRun,
	Flags: []cli.Flag{
		&cli.BoolFlag{
			Name:    "all",
			Aliases: []string{"a"},
			Usage:   "list all the things, even those in sync",
		},
	},
}

func readLock() *LockFile {
	gopath = os.Getenv("GOPATH")
	if gopath == "" {
		log.Fatal("Must set GOPATH")
	}

	buf, err := ioutil.ReadFile("glide.lock")
	if err != nil {
		log.Fatal(err)
	}
	var lck LockFile
	err = yaml.Unmarshal(buf, &lck)
	if err != nil {
		log.Fatal(err)
	}
	return &lck
}

func statusRun(c *cli.Context) error {
	all := c.Bool("all")
	lck := readLock()
	log.Debug("Hash %s", lck.Hash)

	for _, imp := range lck.Imports {
		st, err := check(&imp)
		if err != nil {
			if err == ErrNoDirectory {
				fmt.Printf("MISSING  %.7s                     %s\n",
					imp.Version,
					imp.Name)
			} else {
				fmt.Printf("*ERROR*  %.7s                     %s\n",
					imp.Version,
					imp.Name)
			}

		} else {
			pristine := st.pristine()
			status := "ok"
			if !pristine {
				if len(st.Changes) > 0 {
					status = "modified"
				} else if st.Behind > 0 {
					status = "behind"
				} else if st.Ahead > 0 {
					status = "ahead"
				} else {
					status = "?"
				}
			}
			if all || !pristine {
				edit := " "
				if len(st.Changes) > 0 {
					edit = "*"
				}
				fmt.Printf("%-8s %.7s %.7s %+4d %+4d %s %s\n",
					status,
					st.Declared.Version,
					st.Actual,
					st.Ahead,
					-st.Behind,
					edit,
					st.Declared.Name)
			}
		}
	}

	return nil
}

type Status struct {
	Declared      *Imported
	Actual        string
	Changes       []GitStatus
	Ahead, Behind int
}

func (st *Status) pristine() bool {
	if st.Declared.Version != st.Actual {
		return false
	}
	if len(st.Changes) > 0 {
		return false
	}
	if st.Ahead != 0 || st.Behind != 0 {
		return false
	}
	return true
}

func check(imp *Imported) (*Status, error) {
	dir := path.Join(gopath, "src", imp.Name)
	_, err := os.Stat(dir)
	if err != nil && os.IsNotExist(err) {
		return nil, ErrNoDirectory
	}

	cmd := exec.Command("git", "rev-parse", "HEAD")
	cmd.Dir = dir
	buf, err := cmd.Output()
	if err != nil {
		return nil, err
	}
	head := strings.TrimSpace(string(buf))

	ch, err := changes(dir)
	if err != nil {
		return nil, err
	}

	ahead, behind, err := dcommit(dir, imp.Version, head)
	return &Status{
		Declared: imp,
		Actual:   head,
		Changes:  ch,
		Ahead:    ahead,
		Behind:   behind,
	}, nil
}

type GitStatus struct {
	X    byte
	Y    byte
	File string
}

// returns the number of commits that x is AHEAD of y, and the number
// that x is BEHIND y
func dcommit(dir, x, y string) (int, int, error) {
	cmd := exec.Command("git", "rev-list", "--left-right", "--count",
		x+"..."+y)
	cmd.Dir = dir
	result, err := cmd.Output()
	if err != nil {
		return 0, 0, err
	}
	out := string(result)
	var a, b int
	n, err := fmt.Sscanf(out, "%d %d", &b, &a)
	if err != nil {
		return 0, 0, err
	}
	if n != 2 {
		return 0, 0, fmt.Errorf("rev-list output %q is malformed", out)
	}
	return a, b, nil
}

func changes(dir string) ([]GitStatus, error) {
	cmd := exec.Command("git", "status", "--porcelain")
	cmd.Dir = dir

	src, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}
	if err := cmd.Start(); err != nil {
		return nil, err
	}

	var st []GitStatus

	lines := bufio.NewScanner(src)
	for lines.Scan() {
		line := lines.Text()
		x := line[0]
		y := line[1]
		file := line[3:]
		k := strings.Index(file, " -> ")
		if k > 0 {
			file = file[:k]
		}
		st = append(st, GitStatus{
			X:    x,
			Y:    y,
			File: file,
		})
	}
	if err := cmd.Wait(); err != nil {
		return nil, err
	}

	if err := lines.Err(); err != nil {
		return nil, err
	}
	return st, nil
}

var ErrNoDirectory = errors.New("no directory")

var checkoutCmd = &cli.Command{
	Name:   "checkout",
	Usage:  "check out missing repositories",
	Action: checkoutRun,
	Flags: []cli.Flag{
		&cli.BoolFlag{
			Name:    "update",
			Aliases: []string{"u"},
			Usage:   "update *existing* repositories to locked version",
		},
		&cli.BoolFlag{
			Name:    "stash",
			Usage:   "stash changes in repositories that are *modified*",
		},
	},
}

func checkoutRun(c *cli.Context) error {
	lck := readLock()
	update := c.Bool("update")
	stash := c.Bool("stash")
	
	for _, imp := range lck.Imports {
		st, err := check(&imp)
		dir := path.Join(gopath, "src", imp.Name)
		docheckout := update

		if err == ErrNoDirectory {
			log.Info("Missing %s", imp.Name)

			err = clone(dir, imp.useRepo())
			if err != nil {
				log.Fatal(err)
			}
			docheckout = true
		} else {
			if len(st.Changes) > 0 {
				if stash {
					log.Info("%s: stashing %d changes",
						imp.Name,
						len(st.Changes))
				} else {
					log.Warning("%s: %d changes in working tree; skipping",
						imp.Name,
						len(st.Changes))
					continue
				}
			}
		}
		if docheckout {
			err = checkout(dir, imp.Version)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
	return nil
}

func clone(dir string, repo string) error {
	cmd := exec.Command("git", "clone", repo, dir)
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func stash(dir string, repo string) error {
	cmd := exec.Command("git", "stash", "save", "WIP stashed by glider")
	cmd.Dir = dir
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func checkout(dir string, version string) error {
	cmd := exec.Command("git", "checkout", version)
	cmd.Dir = dir
	cmd.Stderr = os.Stderr
	return cmd.Run()
}
