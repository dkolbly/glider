Glider: Managing GOPATH using glide.lock
========================================

This tool is designed to work with
[glide](https://github.com/Masterminds/glide) but updating packages in
GOPATH instead of in a vendor directory.  It's useful if you use glide
but prefer to manage repository updates outside of the vendor
directory.
